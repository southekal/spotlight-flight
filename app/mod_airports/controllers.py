from flask import Blueprint, request, render_template, flash, g, session, redirect, url_for, Response, jsonify
from app import logger
from app.airport_codes.helper import AirportCode
from app.api_skyscanner import helper as airport


mod_airports = Blueprint('airport', __name__, url_prefix='')


@mod_airports.route('/airports', methods=['GET'])
# utlizes json file with all airport codes
def airports():
    _a = AirportCode()
    if "query" in request.args:
        logger.info(u'autosuggest search for {}'.format(request.args))
        _data = _a.get_airport_code(dictdata=request.args)
        if _data:
            return jsonify(_data)
        else:
            return jsonify({})
    else:
        return jsonify({})


@mod_airports.route('/airport-data', methods=["GET"])
# utlizes skyscanner api autosuggest service
def airport_data():
    if "query" in request.args:
        logger.info(u'autosuggest search for {}'.format(request.args))
        _ainfo = airport.autosuggest_service(query=request.args["query"])["Places"]
        return jsonify(_ainfo)
    else:
        return jsonify({})

