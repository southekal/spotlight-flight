# -*- coding: UTF-8 -*-
import datetime

# from app import db
from app import conn
from mongoengine import *
from werkzeug import check_password_hash, generate_password_hash


class User(Document):
    email = StringField(required=True, unique=True)
    name = StringField(max_length=50)
    password = StringField(max_length=300)
    created = DateTimeField(default=datetime.datetime.now)
    paid_subscriber = BooleanField(default=False)
    active_until = DateTimeField(null=True)
    alert_subscribed = BooleanField(default=True)

    def is_active(self):
        """True, as all users are active."""
        return True

    def get_id(self):
        """Return the email address to satisfy Flask-Login's requirements."""
        return self.email

    def is_authenticated(self):
        """Return True if the user is authenticated."""
        return self.authenticated

    def is_anonymous(self):
        """False, as anonymous users aren't supported."""
        return False


class StripeCustomer(Document):
    user = ReferenceField(User, reverse_delete_rule=CASCADE)
    customer_id = StringField(required=True)
    card_id = StringField()
    subscription_id = StringField()
    last_four = StringField()
    expiration_month = StringField()
    expiration_year = StringField()
    zipcode = StringField()
    created = DateTimeField(default=datetime.datetime.now)


class FlightUserProfileDb(Document):
    user = ReferenceField(User, reverse_delete_rule=CASCADE)
    departure = StringField()
    arrival = StringField()
    dates = StringField()
    status = BooleanField(default=True)
    created = DateTimeField(default=datetime.datetime.now)


class OneSignalData(Document):
    user = ReferenceField(User, reverse_delete_rule=CASCADE)
    onesignal_id = StringField()
    created = DateTimeField(default=datetime.datetime.now)


class PasswordHashing(object):
    def __init__(self, password):
        self.password = password

    def set_password(self):
        pw_hash = generate_password_hash(self.password)
        return pw_hash

    def check_password(self, pw_hash):
        return check_password_hash(pw_hash, self.password)

