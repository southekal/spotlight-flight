# -*- coding: UTF-8 -*-
from flask import Blueprint, request, render_template, flash, g, session, redirect, url_for
from werkzeug import check_password_hash, generate_password_hash
from flask_login import LoginManager, login_user, logout_user, current_user, login_required


mod_landing = Blueprint('landing', __name__, url_prefix='')


@mod_landing.route('/about', methods=['GET', 'POST'])
def about():
    return render_template("landing/about.html")
