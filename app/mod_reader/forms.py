# -*- coding: UTF-8 -*-
from flask_wtf import FlaskForm
from wtforms import Form, BooleanField, StringField, PasswordField, TextField
from wtforms.validators import DataRequired


class DataSearcher(FlaskForm):
    search_arrival = StringField('search_arrival')
    search_departure = StringField('search_departure')
    search_title = StringField('search_title')
    search_airlines = StringField('search_airlines')
    search_price = StringField('search_price')
    search_dates = StringField('search_dates')



