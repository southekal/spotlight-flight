# -*- coding: UTF-8 -*-
from flask_login import LoginManager, login_user, logout_user, current_user, login_required
from fuzzywuzzy import fuzz
from app.mod_reader.models import FlightData
from app import logger
from mongoengine.queryset.visitor import Q


def search_all(*args, **kwargs):
    fd = FlightData.objects(Q(arrival__icontains=kwargs["arrival"]) &
                            Q(departure__icontains=kwargs["departure"]) &
                            Q(title__icontains=kwargs["title"]) &
                            Q(airlines__icontains=kwargs["airlines"]) &
                            Q(dates__icontains=kwargs["dates"]) &
                            Q(status=True))

    return fd


def search_arrival_departure(*args, **kwargs):
    fd = FlightData.objects(Q(arrival__icontains=kwargs["arrival"]) &
                            Q(departure__icontains=kwargs["departure"]) &
                            Q(status=True))

    return fd


def fuzzy_search_arrival_departure(*args, **kwargs):
    # http://chairnerd.seatgeek.com/fuzzywuzzy-fuzzy-string-matching-in-python/
    # kwargs = data from alerts
    # fd/_f = flight data stored
    fd = FlightData.objects(status=True)
    fuzzy_ratio = 60

    matching_flights = []
    for _f in fd:
        if kwargs["arrival"] and kwargs["departure"]:
            arr_ratio = fuzz.token_set_ratio(_f["arrival"], kwargs["arrival"])
            dep_ratio = fuzz.token_set_ratio(_f["departure"], kwargs["departure"])
            if arr_ratio >= fuzzy_ratio \
                    and dep_ratio >= fuzzy_ratio:
                logger.info(u'score {}; {} match found for {}; {}; {}; {}'.format(
                    arr_ratio,
                    dep_ratio,
                    kwargs["arrival"],
                    kwargs["departure"],
                    _f["arrival"],
                    _f["departure"]))
                matching_flights.append(_f)

        elif kwargs["arrival"] and not kwargs["departure"]:
            arr_ratio = fuzz.token_set_ratio(_f["arrival"], kwargs["arrival"])
            if arr_ratio >= fuzzy_ratio:
                logger.info(u'score {} match found for {}; {};'.format(
                    arr_ratio,
                    kwargs["arrival"],
                    _f["arrival"]))
                matching_flights.append(_f)

        elif kwargs["departure"] and not kwargs["arrival"]:
            dep_ratio = fuzz.token_set_ratio(_f["departure"], kwargs["departure"])
            if dep_ratio >= fuzzy_ratio:
                logger.info(u'score {} match found for {}; {};'.format(
                    dep_ratio,
                    kwargs["departure"],
                    _f["departure"]))
                matching_flights.append(_f)

    return matching_flights


def search_arrival(search_term):
    fd = FlightData.objects(Q(arrival__icontains=search_term) &
                            Q(status=True))
    return fd


def search_departure(search_term):
    fd = FlightData.objects(Q(departure__icontains=search_term) &
                            Q(status=True))
    return fd


def search_title(search_term):
    fd = FlightData.objects(Q(title__icontains=search_term) &
                            Q(status=True))
    return fd


def search_airlines(search_term):
    fd = FlightData.objects(Q(airlines__icontains=search_term) &
                            Q(status=True))
    return fd


def search_dates(search_term):
    fd = FlightData.objects(Q(dates__icontains=search_term) &
                            Q(status=True))
    return fd


def search_price(search_term):
    fd = FlightData.objects(Q(price__icontains=search_term) &
                            Q(status=True))
    return fd


def remove_priceline_deals(flight_data):
    priceline_url = "http://www.dpbolvw.net"
    all_flight_deals = [x for x in flight_data if x.deal_url]
    filtered_flight_deals = list(filter(lambda x: priceline_url not in x.deal_url, all_flight_deals))
    logger.info(u"removed {} priceline deals".format(len(filtered_flight_deals)))
    return filtered_flight_deals


def remove_priceline_search_departure(*args, **kwargs):
    flight_data = fuzzy_search_arrival_departure(*args, **kwargs)
    filtered_flight_deals = remove_priceline_deals(flight_data)
    return filtered_flight_deals
