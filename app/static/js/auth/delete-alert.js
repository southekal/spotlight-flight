$(function() {
    $('button#delete_alert_ajax').bind('click', function() {
        var data = { id_param: $(this).closest('div').attr('id')};
        $.ajax({
            type: "POST",
            url: $SCRIPT_ROOT + '/remove-flight-preferences',
            data: JSON.stringify(data),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function(data, textStatus, jqXHR)
            {
//                console.log(data);
                location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                console.log(errorThrown);
            }
        });

        return true;
    });
});