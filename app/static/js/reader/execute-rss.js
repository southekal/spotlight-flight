$(function() {
    $('button#execute_rss_feed_ajax').bind('click', function() {
        var data = { msg: 'parse rss' };
        var newData = [];
        newData.push('Executing RSS Scraper...');

        $('#execute_rss_feed_ajax').removeClass('btn-success');
        $('#execute_rss_feed_ajax').addClass('btn-danger');
        $("#execute_rss_feed_ajax").html(newData.join(""));

        $.ajax({
            type: "POST",
            url: $SCRIPT_ROOT + '/data-parser',
            data: JSON.stringify(data),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function(data, textStatus, jqXHR)
            {
                console.log("success");
                location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                console.log(errorThrown);
            }
        });

        return true;
    });
});