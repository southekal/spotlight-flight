# -*- coding: UTF-8 -*-
from flask import Blueprint, Markup, request, render_template, flash, g, session, redirect, url_for, jsonify
from flask_login import LoginManager, login_user, logout_user, current_user, login_required
from werkzeug.exceptions import HTTPException, NotFound
import os
import json
import validators
import datetime
import stripe

from app import app
from app.mod_auth.forms import SignUp, Login, FlightUserProfile

from app.mod_auth.models import User, StripeCustomer, PasswordHashing, FlightUserProfileDb, OneSignalData
from app.mod_reader.models import FlightData
from app.mod_auth.helper import dbhelper, onesignal_notification
from app.mod_reader.helper import dbhelper as readerhelper
from app.email_sender.helper import send_email

mod_auth = Blueprint('auth', __name__, url_prefix='')

from app import login_manager
from app import logger

stripe_keys = {'secret_key': os.environ["STRIPE_SECRET_KEY"],
               'publishable_key': os.environ["STRIPE_PUBLISHABLE_KEY"]}

stripe_plans = {"stripe_economy_plan": "economy-plan",
                "stripe_first_class_plan": "first-class-plan",
                "stripe_test_plan": "test-plan"}

# $9/mo plan
STRIPE_PLAN_AMOUNT = 900

# JSON CONTENT FILE
content = None
json_filename = os.path.join(app.static_folder, 'templates_config/index.json')
with open(json_filename) as index_file:
    content = json.load(index_file)


@login_manager.user_loader
def load_user(email):
    try:
        # return User.objects.get_or_404(email=email)
        return User.objects(email=email)[0]
    except NotFound as e:
        logger.error('could not load user - "{}"'.format(e))
        return "404.html", 404


@mod_auth.route('/', methods=['GET', 'POST'])
def index():
    r = readerhelper.remove_priceline_deals(FlightData.objects(status=True))
    flight_data = None
    if len(r) > 0:
        flight_data = r
    return render_template("index.html", email=current_user.get_id(), content=content, flight_data=flight_data)


@mod_auth.route('/signup', methods=['GET', 'POST'])
def signup():
    email_placeholder = None
    name_placeholder = None
    if "email" in request.args:
        email_placeholder = request.args["email"]
    if "name" in request.args:
        name_placeholder = request.args["name"]
    if current_user.is_authenticated:
        flash("You are already signed in!", "success")
        return redirect('/')
    form = SignUp(request.form)
    if request.method == "POST" and form.validate_on_submit():

        # checks of email structure is valid
        is_valid_email = validators.email(form.email.data)
        if not dbhelper.user_exists(email=form.email.data) and is_valid_email:
            _hash = PasswordHashing(form.password.data).set_password()
            logger.info('signing up user {}'.format(form.email.data))
            # create the user in the database
            u = User(email=form.email.data, name=form.name.data, password=_hash).save()

            # user authentication
            login_user(u)

            # send welcome email
            send_email(
                recipient_list=[form.email.data],
                html=render_template("email/signup.html", name=form.name.data),
                subject="Welcome to SpotlightFlight. We're glad you found us"
            )

            return redirect('/')
        elif not is_valid_email:
            flash("email is invalid", "danger")
        else:
            flash("Email already exists! <b><a href='/login'>Login</a></b> to access it", "danger")
    return render_template("auth/signup.html",
                           form=form,
                           email_placeholder=email_placeholder,
                           name_placeholder=name_placeholder)


@mod_auth.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect("/")

    form = Login(request.form)
    if request.method == "POST" and form.validate_on_submit():
        _user_exists = dbhelper.user_exists(email=form.email.data)
        if _user_exists:
            _check_hash = dbhelper.password_match(form.password.data, form.email.data)
            if _check_hash:
                logger.info('logging in user {}'.format(form.email.data))
                u = User.objects(email=form.email.data)
                # user authentication
                login_user(u[0])
                flash('Logged in successfully!', 'success')
                return redirect('/')
            else:
                flash('Credentials do not match!', 'danger')
        else:
            flash('User does not exist! Create an account <b><a href="/signup">here</a></b>', "danger")
    return render_template("auth/login.html", form=form)


@mod_auth.route('/profile', methods=['GET', 'POST'])
def profile():
    if not current_user.is_authenticated:
        flash(u"Login to view your profile!", "danger")
        return redirect('/login')
    if "status" in request.args:
        flash(request.args["status"], "success")
    user = User.objects(email=current_user.get_id())[0]
    customer_data = StripeCustomer.objects(user=User.objects(email=current_user.get_id())[0])
    return render_template("auth/profile.html",
                           user=user,
                           customer_data=customer_data,
                           content=content,
                           current_time = datetime.datetime.now(),
                           stripe_key=stripe_keys['publishable_key'])


@mod_auth.route('/flight-preferences', methods=['GET', 'POST'])
def flight_preferences():
    if not current_user.is_authenticated:
        flash(u"Sign up to create flight alerts!", "danger")
        return redirect('/signup')
    form = FlightUserProfile(request.form)

    user_obj = User.objects(email=current_user.get_id())[0]
    alerts_created, matching_flight_alerts = dbhelper.find_matching_alerts(user_obj=user_obj)

    is_maxed_basic_plan_user = dbhelper.is_maxed_basic_plan_user(user_obj)

    if request.method == "POST" and (form.arrival.data or form.departure.data):
        logger.info(u'saving flight user profile alert to db')
        f = FlightUserProfileDb(user=User.objects(email=current_user.get_id())[0],
                                arrival=form.arrival.data,
                                departure=form.departure.data,
                                dates=form.dates.data)
        f.save()
        logger.info(u'successfully saved flight user alert profile')
        # reload the page
        return redirect(url_for('auth.flight_preferences'))

    elif request.method == "POST" and not (form.arrival.data or form.departure.data):
        flash('Departure or Arrival is required to save an alert', category="danger")

    return render_template("auth/flight_preferences.html",
                           form=form, data=alerts_created,
                           matching_flight_alerts=matching_flight_alerts,
                           content=content,
                           is_maxed_basic_plan_user=is_maxed_basic_plan_user)


@mod_auth.route('/remove-flight-preferences', methods=['POST'])
@login_required
def remove_flight_preferences():
    id_param = None
    if request.method == "POST":
        id_param = request.json["id_param"]
        logger.info(u'deactivating flight alert with id: "{}"'.format(id_param))
        d = FlightUserProfileDb.objects(id=id_param)[0]
        nd = FlightUserProfileDb(id=id_param,
                                 user=User.objects(email=current_user.get_id())[0],
                                 arrival=d["arrival"],
                                 departure=d["departure"],
                                 dates=d["dates"],
                                 status=False)

        nd.save()
        logger.info(u'deactivated flight alert with id: "{}" successfully'.format(id_param))
    return jsonify({"data": id_param})


@mod_auth.route('/save-onesignal-data', methods=['POST'])
@login_required
def save_onesignal_data():
    onesignal_id = None
    if request.method == "POST":
        onesignal_id = request.json["onesignal_id"]

        if dbhelper.onesignal_id_exists(onesignal_id):
            logger.info(u'onesignal data id - "{}" already exists'.format(onesignal_id))
        else:
            logger.info(u'saving onesignal data id - "{}" and email - "{}"'.format(onesignal_id, current_user.get_id()))
            od = OneSignalData(user=User.objects(email=current_user.get_id())[0], onesignal_id=onesignal_id)
            od.save()
            
    return jsonify({"data": onesignal_id})


@mod_auth.route('/send-onesignal-alert', methods=['POST'])
@login_required
def send_onesignal_alert():
    onesignal_id = request.json["onesignal_id"]
    user_obj = User.objects(email=current_user.get_id())[0]
    alerts_created, matching_flight_alerts = dbhelper.find_matching_alerts(user_obj=user_obj)

    if matching_flight_alerts:
        titles = [x["title"] for x in matching_flight_alerts]
        message_string = "\n\n".join(titles).encode('utf-8').strip()
        onesignal_notification.send_notification(
            player_id_list=[str(onesignal_id)],
            message="{}!".format(message_string))

    return jsonify({"data": onesignal_id})


@mod_auth.route('/paywall', methods=['GET', 'POST'])
@login_required
def paywall():
    if request.method == "POST" and request.form["stripeToken"]:
        stripe.api_key = stripe_keys['secret_key']
        token_id = request.form["stripeToken"]
        amount = STRIPE_PLAN_AMOUNT

        try:
            # create a customer
            customer = stripe.Customer.create(
                email=current_user.get_id(),
                source=token_id
            )

            logger.info(u"customer data {}".format(customer))

            # add customer to the subscription plan
            subscription = stripe.Subscription.create(
                customer=customer.id,
                metadata={"spotlightflight_section": "paywall"},
                plan=stripe_plans["stripe_first_class_plan"],
            )

            logger.info(u"subscription data {}".format(subscription))
            logger.info(u"token generated - {}; customer_id - {};".format(token_id, customer.id))

            # store stripe customer data in database to be re-used
            sc = StripeCustomer(user=User.objects(email=current_user.get_id())[0],
                                customer_id=customer.id,
                                card_id=customer["sources"]["data"][0]["id"],
                                subscription_id = subscription.id,
                                last_four=customer["sources"]["data"][0]["last4"],
                                expiration_month=str(customer["sources"]["data"][0]["exp_month"]),
                                expiration_year=str(customer["sources"]["data"][0]["exp_year"]),
                                zipcode=customer["sources"]["data"][0]["address_zip"]
                                )

            sc.save()

            # set user as paid_subscriber=True
            # add a timedelta of 31 days
            active_until = datetime.datetime.now() + datetime.timedelta(days=31)
            User.objects(email=current_user.get_id()).update_one(set__paid_subscriber=True,
                                                                 set__active_until=active_until)
            logger.info(u"customer data saved into the db")

        except (stripe.error.CardError,
                stripe.error.AuthenticationError,
                stripe.error.APIConnectionError,
                stripe.error.APIError,
                stripe.error.PermissionError,
                stripe.error.StripeError) as e:
            flash(u"Your card was declined. Please contact us to resolve it", category="danger")
            logger.error(u"Stripe Error {}".format(e))

        return redirect(url_for('auth.flight_preferences'))
    else:
        logger.error(u"POST did not contain STRIPE token")
        return jsonify({"data": "POST did not contain STRIPE token"})


@mod_auth.route("/update-card", methods=['POST'])
@login_required
def update_card():
    stripe.api_key = stripe_keys['secret_key']
    token_id = request.form["stripeToken"]
    customer_data = StripeCustomer.objects(user=User.objects(email=current_user.get_id())[0])

    cu = stripe.Customer.retrieve(customer_data["customer_id"])
    card = cu.sources.create(source=token_id)

    # GET NEW CARD ID
    new_card_id = card.id
    # SET CUSTOMER'S NEW CARD ID TO TO DEFAULT
    cu.default_source = new_card_id
    # SAVE NEW CARD
    cu.save()

    new_cu = stripe.Customer.retrieve(customer_data["customer_id"])
#     update Customer object
#     StripeCustomer.objects(email=current_user.get_id()).update_one(set__paid_subscriber=False)

    return jsonify({"data": "card updated"})


@mod_auth.route("/delete-card", methods=['POST'])
@login_required
def delete_card():
    stripe.api_key = stripe_keys['secret_key']
    customer_data = StripeCustomer.objects(user=User.objects(email=current_user.get_id())[0])

    cu = stripe.Customer.retrieve(customer_data["customer_id"])
    cu.delete()

    # delete customer record
    StripeCustomer.objects(user=User.objects(email=current_user.get_id())[0]).delete()
#   mark user DB - paid_subscriber = False
    User.objects(email=current_user.get_id()).update_one(set__paid_subscriber=False)

    logger.info('deleted customer record of user {}'.format(current_user.get_id()))
    return jsonify({"data": "card deleted"})


@mod_auth.route("/cancel-subscription", methods=['POST'])
@login_required
def cancel_subscription():
    stripe.api_key = stripe_keys['secret_key']
    customer_data = StripeCustomer.objects(user=User.objects(email=current_user.get_id())[0])[0]
    try:
        sub = stripe.Subscription.retrieve(customer_data["subscription_id"])
        # cancel the subscription at the end of the current billing period
        sub.delete(at_period_end=True)

        # mark user DB - paid_subscriber = False
        User.objects(email=current_user.get_id()).update_one(set__paid_subscriber=False)
        logger.info(u'cancelled subscription of customer {} of user {}'.format(customer_data["customer_id"], current_user.get_id()))

        # delete customer record
        StripeCustomer.objects(user=User.objects(email=current_user.get_id())[0]).delete()
        logger.info(u'deleted customer record')
    except AttributeError as e:
        logger.error(e)

    return jsonify({"data": "subscription cancelled"})


@mod_auth.route("/stripe-webhook", methods=['POST'])
def stripe_webhook():
    if request.method == "POST":
        event_json = request.json

        logger.info("stripe webhook event_type {}".format(event_json["type"]))
        logger.info("stripe webhook customer_id {}".format(event_json["data"]["object"]["customer"]))

        event_types = ["invoice.payment_succeeded", "invoice.payment_failed", "customer.subscription.deleted"]

        if event_json["type"] in event_types:
            logger.info(u"stripe data {}".format(event_json))
            customer_data = StripeCustomer.objects(customer_id=event_json["data"]["object"]["customer"])
            if len(customer_data) > 0:
                if event_json["type"] == "invoice.payment_succeeded":
                    active_until = datetime.datetime.now() + datetime.timedelta(days=31)
                    User.objects(id=customer_data[0]["user"]["id"]).update_one(set__paid_subscriber=True,
                                                                         set__active_until=active_until)
                elif event_json["type"] == "invoice.payment_failed" \
                        or event_json["type"] == "customer.subscription.deleted":
                    User.objects(id=customer_data[0]["user"]["id"]).update_one(set__paid_subscriber=False)

                logger.info(u"customer data saved into the db through stripe webhook")
        return jsonify({"event_data": event_json})
    else:
        return jsonify({"event_data": "only POST is allowed"})


@mod_auth.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect("/")


@mod_auth.route("/.well-known/acme-challenge/<sid>")
def encrypt(sid):
    """
    module used for verification using Let's Encrypt self signed certificate
    eg: https://medium.com/should-designers-code/how-to-set-up-ssl-with-lets-encrypt-on-heroku-for-free-266c185630db#.w20rvgtxp
    :param sid: unique ID generated by Let's Encrypt
    :return: Text
    """
    return "{}.JA_vKAbi12UNuYnfIq4RfEBC2DZXWlmqnqNflvs0Xgg".format(sid)

