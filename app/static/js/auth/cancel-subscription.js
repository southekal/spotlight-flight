$(function() {
    $('button#cancelSubscriptionConfirm').bind('click', function() {
        var data = {"type": "cancel subscription"}
        $.ajax({
            type: "POST",
            url: $SCRIPT_ROOT + '/cancel-subscription',
            data: JSON.stringify(data),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function(data, textStatus, jqXHR)
            {
//                console.log(data);
                location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                console.log(errorThrown);
            }
        });

        return true;
    });
});