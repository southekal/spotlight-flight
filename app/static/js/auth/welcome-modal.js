$(function() {
    if (Cookies.get('welcome') == undefined ) {
        function show_modal() {
            $('#welcomeModal').modal('show');
        }
    }

//    set session cookie if cancel button is clicked
    $(".no_thanks").click(function() {
        Cookies('welcome', 'true', { path: '/' });
    });

    window.setTimeout(show_modal, 2000);

});
