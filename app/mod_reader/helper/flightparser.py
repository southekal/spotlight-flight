# -*- coding: UTF-8 -*-
import sys
import os
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../../..')))

import json
import random
from datetime import datetime

from app import app
from app import logger
from app.mod_reader.models import RSSData, FlightData
from app.mod_reader.helper import rss_parser, dbhelper
from app.email_sender import helper
from app.api_skyscanner import helper as sky_api


# JSON IMAGE DIRECTORY FILE
img_content = None
json_filename = os.path.join(app.static_folder, 'templates_config/image_library.json')
with open(json_filename) as index_file:
    img_content = json.load(index_file)


def parse_flight_data():
    url = "http://www.secretflying.com/feed/"
    rss_data = rss_parser.rparser(url=url)
    cleaned_rss_data = rss_parser.rcleaner(rss_data)

    logger.info(u"saving rss data to database - {}".format(url))
    r = RSSData(url=url,
                content_blob=cleaned_rss_data)
    r.save()
    logger.info(u"rss save complete")

    # set all the previously stored flightdata to status=False
    FlightData.objects().update(set__status=False)

    for _d in cleaned_rss_data:
        logger.info(u"saving flight data - {}".format(_d["title"]))
        flightdata = [rss_parser.get_arrival_departure(x["value"]) for x in _d["content"]][0]

        # grab pricing data from the title
        price = price_assigner(text_to_verify=_d["title"])
        full_price = price[0] + price[1]
        currency = currency_assigner(price[0])

        # assign list of images based on matching arrival/departure
        img_assigned = image_assigner(flightdata=flightdata)

        # find the cheapest price from skyscanner api
        cheapest_flight_price = cheapest_flight_assigner(flightdata=flightdata)

        url = rss_parser.get_deal_url(_d["link"])
        fd = FlightData(rss_data=r,
                        url=_d["link"],
                        deal_url=url,
                        title=_d["title"],
                        price=full_price,
                        digit_price=price[1],
                        currency=currency,
                        arrival=flightdata["arrival"],
                        departure=flightdata["departure"],
                        dates=flightdata["dates"],
                        airlines=flightdata["airlines"],
                        image=img_assigned,
                        cheapest_price=cheapest_flight_price
                        )
        fd.save()
        logger.info(u"flight data save complete")
    helper.job_email_sender(data_stack="Flight Parser Executed - {}".format(datetime.now().time()))


def price_assigner(text_to_verify):
    # pricing parsing
    pricing = rss_parser.get_price(text_to_verify)
    price = None
    if pricing:
        price = pricing
    return price


def currency_assigner(currency_to_convert):

    currency_dict = {u"$": u"USD", u"￥": u"JPY", u"£": u"GBP", u"€": u"EUR"}
    currency = None
    if currency_to_convert in currency_dict:
        currency = currency_dict[currency_to_convert]
    return currency


def image_assigner(flightdata):
    # image assignment based on city of arrival/departure
    try:
        arrival_img = str(flightdata["arrival"]).lower().split(", ")
        departure_img = str(flightdata["departure"]).lower().split(", ")

        if arrival_img[0] in img_content:
            logger.info(u"assigning arrival image for {}".format(arrival_img[0]))
            img_assigned = assign_random_image(img_content[arrival_img[0]])
        elif departure_img[0] in img_content:
            logger.info(u"assigning departure image for {}".format(departure_img[0]))
            img_assigned = assign_random_image(img_content[departure_img[0]])
        else:
            logger.info(
                u"assigning generic image for arrival {}; departure {}".format(arrival_img[0], departure_img[0]))
            img_assigned = assign_random_image(img_content["generic"])

    except UnicodeEncodeError as e:
        logger.error(u"error on saving image for arrival {}; departure {} - {}".
                     format(flightdata["arrival"], flightdata["departure"], e))
        img_assigned = assign_random_image(img_content["generic"])

    return img_assigned


def assign_random_image(img_list):
    # db model for image is a list field
    return [random.choice(img_list)]


def cheapest_flight_assigner(flightdata):
    cheapest_flight = {}
    try:
        arrival = str(flightdata["arrival"]).lower().split(", ")
        departure = str(flightdata["departure"]).lower().split(", ")
        if len(arrival) > 0 and len(departure) > 0:
            cheapest_flight = sky_api.find_cheapest_fare(departure=departure[0], arrival=arrival[0])
    except UnicodeEncodeError as e:
        logger.error(u"error on getting cheapest flight data: arrival {}; departure {} - {}".
                     format(flightdata["arrival"], flightdata["departure"], e))

    return cheapest_flight


if __name__ == "__main__":
    parse_flight_data()



