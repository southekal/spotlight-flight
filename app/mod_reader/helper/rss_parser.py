# -*- coding: UTF-8 -*-
import re
from bs4 import BeautifulSoup
import requests
import feedparser
import pprint
import ConfigParser
from app import logger
from app import exception_handler

# read the fields to be parsed from the RSS file from a config
cfg = ConfigParser.ConfigParser()
cfg.read('./app/mod_reader/helper/rssfields.cfg')

headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36'}


def rparser(url):
    # d = feedparser.parse(self.url)
    r = requests.get(url=url, headers=headers)
    d = feedparser.parse(r.text)
    # pprint.pprint(d)
    return d["items"]


def rcleaner(full_rss):
    arr_holder = []
    section_holder = cfg.get("FIELDS", "FIELDS").split(",")
    for _rss in full_rss:
        json_holder = {}
        for _s in section_holder:
            if _s in _rss:
                json_holder[_s] = _rss[_s]

        arr_holder.append(json_holder)

    return arr_holder


def get_price(text_to_verify):
    try:
        parsed_currency = re.search(ur'([£$€])(\d+(?:\.\d{2})?)', text_to_verify).groups()
    except AttributeError as e:
        logger.error('currency scrape error: {}'.format(e))
        parsed_currency = None
    return parsed_currency


def get_deal_url(deal_link):
    r = requests.get(deal_link, headers=headers)
    soup = BeautifulSoup(r.text, 'html.parser')
    info = soup.find_all("div", class_="btn_cont")
    href = None
    if len(info) > 0:
        href = [x["href"] for x in info[0].find_all('a', href=True)][0]
    return href


def get_arrival_departure(text_to_verify):
    _flightdata = {}
    soup = BeautifulSoup(text_to_verify, 'html.parser')
    info = soup.find_all("p")
    try:
        _flightdata["departure"] = [x.text.strip("DEPART:").split('\n') for x in info if "DEPART" in x.text][0][1]
    except IndexError as e:
        logger.error('departure scrape error: {}'.format(e))
        _flightdata["departure"] = None
    try:
        _flightdata["arrival"] = [x.text.strip("ARRIVE:").split('\n') for x in info if "ARRIVE" in x.text][0][1]
    except IndexError as e:
        logger.error('arrival scrape error: {}'.format(e))
        _flightdata["arrival"] = None
    try:
        _flightdata["dates"] = [x.text.strip("DATES:").split('\n') for x in info if "DATES" in x.text][0][1]
    except IndexError as e:
        logger.error('dates scrape error: {}'.format(e))
        _flightdata["dates"] = None
    try:
        _flightdata["airlines"] = [x.text.strip("AIRLINE:").split('\n') for x in info if "AIRLINE" in x.text][0][1]
    except IndexError as e:
        logger.error('airlines scrape error: {}'.format(e))
        _flightdata["airlines"] = None
    return _flightdata

