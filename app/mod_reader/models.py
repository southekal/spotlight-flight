# -*- coding: UTF-8 -*-
import datetime

# from app import db
from app import conn
from mongoengine import *


class RSSData(Document):
    url = StringField(required=True)
    content_blob = ListField()
    created = DateTimeField(default=datetime.datetime.now)


class FlightData(Document):
    rss_data = ReferenceField(RSSData, reverse_delete_rule=CASCADE)
    url = StringField()
    deal_url = StringField()
    title = StringField()
    departure = StringField()
    arrival = StringField()
    dates = StringField()
    price = StringField()
    digit_price = FloatField()
    currency = StringField()
    airlines = StringField()
    image = ListField()
    cheapest_price = DictField(default={})
    created = DateTimeField(default=datetime.datetime.now)
    status = BooleanField(default=True)
