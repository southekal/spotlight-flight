# -*- coding: UTF-8 -*-
import datetime
from flask_login import LoginManager, login_user, logout_user, current_user, login_required
from app import logger
from app.mod_auth.models import User, PasswordHashing, OneSignalData, FlightUserProfileDb
from app.mod_reader.helper import dbhelper as readerdbhelper


def user_exists(email):
    u = User.objects(email=email)
    if len(u) > 0:
        return True
    else:
        return False


def get_user_details(email):
    fields = ["id", "email", "name", "created", "paid_subscriber"]
    data_holder = {}
    _user_exists = user_exists(email)
    if _user_exists:
        u = User.objects(email=current_user.get_id())[0]
        for f in fields:
            data_holder[f] = u[f]
    return data_holder


def get_user_password(email):
    _user_exists = user_exists(email)
    if _user_exists:
        u = User.objects(email=email)[0]
        return u["password"]
    else:
        return None


def password_match(pw, email):
    _pw = get_user_password(email)
    if _pw:
        p = PasswordHashing(password=pw).check_password(_pw)
        return p
    else:
        return False


def find_matching_alerts(user_obj):
    """
    :param user_obj: Full User Object from User Table
    :return: all alerts created + matching flight deals with alerts
    """
    f = FlightUserProfileDb.objects(user=user_obj, status=True).order_by('-created')
    alerts_created = None
    matching_flight_alerts = []
    if len(f) > 0:
        # only bring back alerts that are active
        alerts_created = f
        for _d in alerts_created:
            matching_flight_alerts.extend(readerdbhelper.remove_priceline_search_departure
                                          (arrival=_d["arrival"], departure=_d["departure"]))

    return alerts_created, set(matching_flight_alerts)


def is_subscribed():
    current_datetime = datetime.datetime.now()
    u = User.objects(email=current_user.get_id())[0]
    if u["paid_subscriber"] or (u["active_until"] is not None and current_datetime < u["active_until"]):
        return True
    else:
        return False


def is_max_alerts(user_obj):
    basic_plan_max_alerts = 3
    f = FlightUserProfileDb.objects(user=user_obj)
    if len(f) > 0:
        alerts_created = [x for x in f if x["status"]]
        if len(alerts_created) >= basic_plan_max_alerts:
            return True
        else:
            return False
    else:
        return False


def is_maxed_basic_plan_user(user_obj):
    return not is_subscribed() and is_max_alerts(user_obj)


def onesignal_id_exists(onesignal_id):
    _o = OneSignalData.objects(onesignal_id=onesignal_id)
    if len(_o) > 0:
        return True
    else:
        return False


