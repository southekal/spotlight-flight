from sparkpost import SparkPost
from sparkpost import SparkPostException
import os
from app import logger

sparky = SparkPost(os.environ["SPARKPOST_API_KEY"])
f_email = 'Spotlight Flight <info@spotlightflight.com>'


def send_email(recipient_list, html, subject, from_email=f_email):

    logger.info(u"sending email with subject '{}' to recipient '{}'".format(subject, recipient_list))
    try:
        response = sparky.transmission.send(
            recipients=recipient_list,
            html=html,
            from_email=from_email,
            subject=subject
        )

        logger.info(u"email response {}".format(response))

    except SparkPostException as e:
        logger.error(u"{}".format(e))


def job_email_sender(data_stack):
    send_email(recipient_list=["southekal@gmail.com"],
               html="<p>{}</p>".format(data_stack),
               subject="Backend Job Status")
