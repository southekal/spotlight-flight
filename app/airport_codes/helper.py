import os
import sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../..')))
import json
from app import logger

JSONFILE_STRUCT = 'app/airport_codes/json/airportdata.json'


class AirportCode(object):
    def __init__(self):
        self.jsonfile = open(JSONFILE_STRUCT, 'r')

    def get_airport_code(self, dictdata):
        _js = json.load(self.jsonfile)

        logger.info(u'airport code request - {}'.format(dictdata))
        for k, v in dictdata.iteritems():
            k = k.lower()
            v = v.lower()
            return [d for d in _js if v in d[k].lower()]


if __name__ == "__main__":
    # testing
    import pprint
    a = AirportCode()
    pprint.pprint(a.get_airport_code(dictdata={"query": "Miami"}))
