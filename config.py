# -*- coding: UTF-8 -*-
import os
import ConfigParser
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = os.environ["SECRET_TOKEN"]
    MONGODB_SETTINGS = {
        'db': os.environ["DB"],
        'host': os.environ["DB_HOST"]
    }
    THREADS_PER_PAGE = 2
    # Use a secure, unique and absolutely secret key for
    # signing the data.
    CSRF_SESSION_KEY = os.environ["CSRF_SECRET_TOKEN"]


class ProductionConfig(Config):
    DEBUG = False
    # change these to PROD values when ready
    os.environ["STRIPE_SECRET_KEY"] = os.environ["STRIPE_SECRET_TEST_KEY"]
    os.environ["STRIPE_PUBLISHABLE_KEY"] = os.environ["STRIPE_PUBLISHABLE_TEST_KEY"]


class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True
    os.environ["STRIPE_SECRET_KEY"] = os.environ["STRIPE_SECRET_TEST_KEY"]
    os.environ["STRIPE_PUBLISHABLE_KEY"] = os.environ["STRIPE_PUBLISHABLE_TEST_KEY"]


class TestingConfig(Config):
    TESTING = True