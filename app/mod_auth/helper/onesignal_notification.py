from app import logger
import os
import requests
import json

ONESIGNAL_BASE_URL = "https://onesignal.com/api/v1/notifications"
ONESIGNAL_API_KEY = os.environ["ONESIGNAL_API_KEY"]

header = {"Content-Type": "application/json; charset=utf-8",
          "Authorization": "Basic {}".format(ONESIGNAL_API_KEY)}

payload = {"app_id": "b24aa9eb-c465-44f0-93f8-e089c421fb9b",
           # "included_segments": ["All"],
           "include_player_ids": ["4c01d511-bcb2-442f-97e7-a32e0bcdbeeb"],
           "contents": {"en": "Hello Sandman! I know you!"}}


def send_notification(player_id_list, message):
    payload["include_player_ids"] = player_id_list
    payload["contents"] = {"en": message}

    req = requests.post(ONESIGNAL_BASE_URL, headers=header, data=json.dumps(payload))

    logger.info('Send OneSignal Notification Status "{}"; Reason "{}"; User "{}"'.
                format(req.status_code, req.reason, player_id_list))
