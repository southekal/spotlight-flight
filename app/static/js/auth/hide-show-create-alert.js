$("#hide-alert, #show-alert").click(function (e) {
    e.preventDefault();
    $("#maximize-create-alert-display").toggle();
    $("#minimize-create-alert-display").toggle();
});
$(".close-alert").click(function (e) {
e.preventDefault();
    $("#maximize-create-alert-display").hide();
    $("#minimize-create-alert-display").hide();
});