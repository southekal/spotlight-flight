# -*- coding: UTF-8 -*-
import os
from flask import Flask, jsonify, request, render_template, redirect, send_from_directory
from flask_login import LoginManager, login_user, logout_user, current_user, login_required
from flask_sslify import SSLify
from flask_mongoengine import MongoEngine
from mongoengine import connect
from werkzeug.exceptions import BadRequest
import logging
from logging.config import fileConfig

fileConfig('app/logging_config.ini')
logger = logging.getLogger()


app = Flask(__name__)
# Flask extension that configures your Flask application to redirect all incoming requests to HTTPS
sslify = SSLify(app)
app.config.from_object(os.environ['APP_SETTINGS'])
# db = MongoEngine(app)
conn = connect(app.config["MONGODB_SETTINGS"]["db"], host=app.config["MONGODB_SETTINGS"]["host"])
logger.info('connected to db: {}'.
            format(app.config["MONGODB_SETTINGS"]["db"]))
# flask authentication
login_manager = LoginManager()
login_manager.init_app(app)


def exception_handler(exceptions=(IndexError, AttributeError, ValueError,
                                  UnicodeDecodeError, UnicodeEncodeError,
                                  SyntaxError)):
    def decorator(f):
        def _wrapper(*args, **kwargs):
            try:
                f(*args, **kwargs)
                return
            except exceptions as err:
                logger.error(err)
        return _wrapper
    return decorator


# Sample HTTP error handling
@app.errorhandler(404)
def not_found(error):
    logger.warning('page not found {} - {}'.format(error, request.url))
    return render_template('404.html'), 404


@app.errorhandler(BadRequest)
def handle_bad_request(e):
    logger.error('bad requests error {}'.format(e))
    return 'bad request!'


# add files for web push notifications
@app.route('/manifest.json')
def manifest_from_root():
    return send_from_directory(app.static_folder, request.path[1:])


@app.route('/OneSignalSDKWorker.js')
def sdk_from_root():
    return send_from_directory(app.static_folder, request.path[1:])


@app.route('/OneSignalSDKUpdaterWorker.js')
def sdk_updater_from_root():
    return send_from_directory(app.static_folder, request.path[1:])
# end of file adds for web push notification


# add path for robots.tx
@app.route('/robots.txt')
def robots_from_root():
    return send_from_directory(app.static_folder, request.path[1:])


# add path for sitemap.xml
@app.route('/sitemap.xml')
def sitemap_from_root():
    return send_from_directory(app.static_folder, request.path[1:])


from app.mod_auth.controllers import mod_auth as auth_module
from app.mod_landing.controllers import mod_landing as landing_module
from app.mod_reader.controllers import mod_reader as data_reader_module
from app.mod_slack.controllers import mod_slack as slack_module
from app.mod_airports.controllers import mod_airports as airport_module

app.register_blueprint(auth_module)
app.register_blueprint(landing_module)
app.register_blueprint(data_reader_module)
app.register_blueprint(slack_module)
app.register_blueprint(airport_module)
