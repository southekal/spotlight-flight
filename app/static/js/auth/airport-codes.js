$( function() {
    $( "#departure, #arrival" ).autocomplete({
        source: function( request, response ){
            $.ajax({
                url: $SCRIPT_ROOT + "/airport-data",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                data: {
                    query: request.term
                },
                success: function(data){
                    response($.map(data, function (el) {
                        return {
                            label: el.PlaceName + ", " + el.CountryName + " (" + el.PlaceId.split("-")[0] + ")",
                            value: el.PlaceName + ", " + el.CountryName + " (" + el.PlaceId.split("-")[0] + ")"
                        };
                    }));
                }
            });
        },
        minLength: 3,
    });
});