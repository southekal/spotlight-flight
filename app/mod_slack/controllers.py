import os
import time
import copy
import requests
import json
from flask import Blueprint, request, render_template, flash, g, session, redirect, url_for, Response, jsonify
from app import logger
from app.mod_reader.models import FlightData
from app.mod_reader.helper import dbhelper
from slackclient import SlackClient

mod_slack = Blueprint('slack', __name__, url_prefix='')

SLACK_WEBHOOK_SECRET = os.environ.get('SLACK_WEBHOOK_SECRET')
client_id = os.environ["SLACK_CLIENT_ID"]
client_secret = os.environ["SLACK_CLIENT_SECRET"]
oauth_scope = os.environ["SLACK_BOT_SCOPE"]

slack_client = SlackClient(os.environ.get("SLACK_API"))

base_url = "https://www.spotlightflight.com"
flight_deals_url = "{}/data-viewer?src=slack".format(base_url)
logo_url = "{}/static/img/nav/nav_logo.png".format(base_url)
color_hash = "#0578bd"
footer_color_hash = "#ff0000"

flight_json_struct = {
    'fallback': u"<{}|Check out some outrageous flight deals for you>".format(flight_deals_url),
    'color': color_hash,
    "fields": [
        {
            "title": "Arrival",
            "value": "",
            "short": True
        },
        {
            "title": "Departure",
            "value": "",
            "short": True
        },
        {
            "title": "Price",
            "value": "",
            "short": True
        },
        {
            "title": "Airlines",
            "value": "",
            "short": True
        },
    ],
    'thumb_url': logo_url,
    'footer': 'Spotlight Flight',
    'footer_icon': logo_url,
    'ts': time.time(),
}

generic_json_struct = {
    'response_type': 'ephemeral',
    'attachments': [
        {
            'fallback': "",
            'color': color_hash,
            'fields': [
                {
                    'title': ""
                }
            ],
            'thumb_url': logo_url,
            'footer': 'Spotlight Flight',
            'footer_icon': logo_url,
            'ts': time.time(),
        }
    ]
}


footer_json_struct = {
    'text': u"<{}|View more outrageous flight deals for you>".format(flight_deals_url),
    'color': footer_color_hash
    }


@mod_slack.route('/add-slack', methods=['GET', 'POST'])
def add_slack():
    return render_template("slack/slack_welcome.html")


@mod_slack.route('/slack', methods=['POST'])
def slack():
    logger.info(u"slack webhook data received: {}".format(request.form))
    if request.form.get("token", None) == SLACK_WEBHOOK_SECRET:
        response_url = request.form.get('response_url', None)
        channel_id = request.form.get('channel_id', None)
        channel = request.form.get('channel_name', None)
        username = request.form.get('user_name', None)
        text = str(request.form.get('text', None)).lower()

        logger.info(u"slack data captured - {}; {}; {}; {}".format(channel_id, channel, username, text))

        try:
            if text == "deals":
                # send an intermediate message
                post_intermediate_message(response_url)
                # generate flight deals in the structure needed
                data_holder = generate_flight_deals()

                return jsonify({
                    'response_type': 'in_channel',
                    'text': "@{} here are today's outrageous flight deals:\n".format(str(username)),
                    'attachments': data_holder

                })

            else:
                message = 'Try asking "Deals" for the latest flight deals or, "Arrival Los Angeles/ Departure New York"'
                json_struct = copy.deepcopy(generic_json_struct)
                json_struct["attachments"][0]["fallback"] = message
                json_struct["attachments"][0]["fields"][0]["title"] = message
                json_struct["attachments"][0]["ts"] = time.time()
                return jsonify(json_struct)

        except:
            logger.error("Something went wrong with Slack slash command. Investigate")
            message = 'I seem to be having issues :disappointed: Can you try chatting with me a bit later please?'
            err_json_struct = copy.deepcopy(generic_json_struct)
            err_json_struct["attachments"][0]["fallback"] = message
            err_json_struct["attachments"][0]["fields"][0]["title"] = message
            err_json_struct["attachments"][0]["ts"] = time.time()
            return jsonify(err_json_struct)
    else:
        logger.info(u"post message was not from slack")

    return Response(), 200


@mod_slack.route("/slack_auth", methods=["GET", "POST"])
def post_slack_install():
    logger.info(u"slack auth request coming in: {}".format(request.args))
    if "error" in request.args:
        flash("{}".format(request.args['error']), category="danger")
        return redirect('/add-slack')

    # Retrieve the auth code from the request params
    auth_code = request.args['code']

    # Request the auth tokens from Slack
    auth_response = slack_client.api_call(
        "oauth.access",
        client_id=client_id,
        client_secret=client_secret,
        code=auth_code
        )

    logger.info(u"slack auth data received: {}".format(auth_response))
    # Set environment variables to make requests
    os.environ["SLACK_USER_TOKEN"] = auth_response['access_token']
    # XXX: Bot is not required
    # os.environ["SLACK_BOT_TOKEN"] = auth_response['bot']['bot_access_token']

    flash("Authorization is complete!", category="success")
    return redirect('/add-slack')


def list_channels():
    channels_call = slack_client.api_call("channels.list")
    if channels_call.get('ok'):
        return channels_call['channels']
    return None


def channel_info(channel_id):
    channel_data = slack_client.api_call("channels.info", channel=channel_id)
    if channel_data:
        return channel_data['channel']
    return None


def send_message(channel_id, message):
    slack_client.api_call(
        "chat.postMessage",
        channel=channel_id,
        text=message,
        username='spotlightflight',
        icon_emoji=':spotlightflight:',
        attachments=[{"title": "View More Outrageous Flight Deals.",
                      "title_link": "{}".format(base_url),
                      "color": "#0578bd",
                      "image_url": logo_url
                      }]
    )


def generate_flight_deals():
    # gets the latest flight deals
    r = dbhelper.remove_priceline_deals(FlightData.objects(status=True))
    data_holder = []
    # return only limited number of deals
    for i in r[:3]:
        x = copy.deepcopy(flight_json_struct)
        x["fields"][0]["value"] = u"{}".format(i["arrival"])
        x["fields"][1]["value"] = u"{}".format(i["departure"])
        x["fields"][2]["value"] = u"<{}|{} roundtrip/person>".format(flight_deals_url, i["price"])
        x["fields"][3]["value"] = u"{}".format(i["airlines"])
        x["ts"] = time.time()
        data_holder.append(x)
    data_holder.append(footer_json_struct)

    return data_holder


def post_intermediate_message(response_url):
    headers = {'content-type': 'application/json'}
    data_payload = {
        "response_type": "ephemeral",
        "text": "Hi! Finding you the 3 best deals for today. "
                "Hope I can make traveling on a budget easy :airplane:",
    }
    logger.info(u"posting to intermediate url {}".format(response_url))
    r = requests.post(response_url, data=json.dumps(data_payload), headers=headers)
    logger.info(u"post request data {}".format(r.text))
    # respond back to slack quickly
    return Response(), 200

