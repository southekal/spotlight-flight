var stripeKey = $('#customStripeButton').data("key");
var handler = StripeCheckout.configure({
    key: stripeKey,
    image: 'https://spotlightflight.herokuapp.com/static/img/SpotlightFlights_Wings.png',
    locale: 'auto',
    token: function(token) {
    // You can access the token ID with `token.id`.
    // Get the token ID to your server-side code for use.
    $.ajax({
        type: "POST",
        url: $SCRIPT_ROOT + '/paywall',
        data: JSON.stringify({ token_id: token.id }),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function(data, textStatus, jqXHR)
        {
                console.log(data);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            console.log(errorThrown);
        }
    });
  }
});

document.getElementById('customStripeButton').addEventListener('click', function(e) {
  // Open Checkout with further options:
  handler.open({
    name: 'Spotlight Flight',
    description: 'Save 80% on International Flights',
    amount: 999,
    zipcode: "true"
  });
  e.preventDefault();
});

// Close Checkout on page navigation:
window.addEventListener('popstate', function() {
  handler.close();
});