import sys
import os
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../../..')))

from jinja2 import Environment, PackageLoader
from app import logger
from app.mod_auth.models import User
from app.mod_auth.helper import dbhelper as authhelper
from app.email_sender.helper import send_email


def get_all_users():
    logger.info(u"gathering all users with active subscription")
    user_obj = User.objects(alert_subscribed=True)
    return user_obj


def send_alert_email(recipient_email, recipient_name, matching_flight_alerts):
    env = Environment(loader=PackageLoader('app', 'templates'))
    alert_template = env.get_template("email/alert_send.html")
    send_email(
        recipient_list=[recipient_email],
        html=alert_template.render(name=recipient_name, matching_flight_alerts=matching_flight_alerts),
        subject=u"Don't miss out on amazing flight deals!"
    )


def email_matching_alerts():
    user_obj = get_all_users()

    # iterate through every user to check for matching alerts
    for _obj in user_obj:
        alerts_created, matching_flight_alerts = authhelper.find_matching_alerts(user_obj=_obj)
        if matching_flight_alerts:
            logger.info(u"sending matching alerts email to user - {}".format(_obj.email))
            send_alert_email(recipient_email=_obj.email,
                             recipient_name=_obj.name,
                             matching_flight_alerts=matching_flight_alerts)

if __name__ == "__main__":
    email_matching_alerts()