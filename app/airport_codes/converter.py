import sys
import os
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../..')))
import csv
import json
from datetime import datetime

d = datetime.now()

DATFILE_STRUCT = 'app/airport_codes/dat/{}.dat'
JSONFILE_STRUCT = 'app/airport_codes/json/airportdata.json'

r = raw_input('enter dat file name: ')
datfile = open(DATFILE_STRUCT.format(r), 'r')
jsonfile = open(JSONFILE_STRUCT, 'w')

csvReader = csv.reader(datfile)
json_arr = []
for row in csvReader:
    json_holder = dict()
    json_holder['altitude'] = row[8]
    json_holder['city'] = row[2]
    json_holder['country'] = row[3]
    json_holder['dst'] = row[10]
    json_holder['iata'] = row[4]
    json_holder['icao'] = row[5]
    json_holder['latitude'] = row[6]
    json_holder['longitude'] = row[7]
    json_holder['name'] = row[1]
    json_holder['timezone'] = row[9]
    json_holder['query'] = json_holder['name'] + ", " + \
                           json_holder['city'] + ", " + \
                           json_holder['country'] + ", " + \
                           json_holder["iata"]
    json_arr.append(json_holder)

json.dump(json_arr, jsonfile)
datfile.close()
jsonfile.close()

