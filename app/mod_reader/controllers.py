# -*- coding: UTF-8 -*-
from flask import Blueprint, Markup, request, render_template, flash, g, session, redirect, url_for, jsonify
from flask_login import LoginManager, login_user, logout_user, current_user, login_required
from collections import OrderedDict

from app import logger
from app.mod_reader.models import RSSData, FlightData
from app.mod_reader.forms import DataSearcher
from app.mod_reader.helper import rss_parser, dbhelper, flightparser

mod_reader = Blueprint('rss', __name__, url_prefix='')


@mod_reader.route('/data-viewer', methods=['GET', 'POST'])
def data_viewer():
    r = dbhelper.remove_priceline_deals(FlightData.objects(status=True))
    expired_deals = dbhelper.remove_priceline_deals(FlightData.objects(status=False).order_by('-created'))[:3]
    data = None
    if len(r) > 0:
        data = r
    else:
        flash(u"no data available yet!", "danger")

    return render_template("user/data_viewer.html",
                           data=data,
                           expired_deals=expired_deals,
                           data_holder=OrderedDict([("RECENT FLIGHT DEALS", data), ("OLDER DEALS", expired_deals)])
                           )


@mod_reader.route('/data-searcher', methods=['GET', 'POST'])
def data_searcher():
    form = DataSearcher(request.form)
    data = {}
    if request.method == "POST" and form.validate_on_submit():
        _attributes = ["search_arrival", "search_departure", "search_airlines", "search_title", "search_price", "search_dates"]
        for a in _attributes:
            # eg: dbhelper.search_arrival(form.search_arrival.data)
            if getattr(getattr(form, a), "data"):
                data[a] = getattr(dbhelper, a)(getattr(getattr(form, a), "data"))

        data["search_all"] = dbhelper.search_all(arrival=form.search_arrival.data,
                                                 departure=form.search_departure.data,
                                                 airlines=form.search_airlines.data,
                                                 title=form.search_title.data,
                                                 dates=form.search_dates.data)

        data["search_arrival_departure"] = dbhelper.search_arrival_departure(arrival=form.search_arrival.data,
                                                                             departure=form.search_departure.data)
    return render_template("user/data_searcher.html", form=form, data=data)
