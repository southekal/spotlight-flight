function sendOneSignalNotification(app_id) {
    var data = { onesignal_id: app_id };
    $.ajax({
        type: "POST",
        url: $SCRIPT_ROOT + '/send-onesignal-alert',
        data: JSON.stringify(data),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function(data, textStatus, jqXHR)
        {
                console.log(data);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            console.log(errorThrown);
        }
    });

    return true;
};