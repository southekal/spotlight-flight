# -*- coding: UTF-8 -*-
from flask_wtf import FlaskForm
from wtforms import Form, BooleanField, StringField, PasswordField, TextField
from wtforms.validators import DataRequired


class SignUp(FlaskForm):
    name = StringField('name', validators=[DataRequired()])
    email = TextField('email', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])


class Login(FlaskForm):
    email = TextField('email', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])


class FlightUserProfile(FlaskForm):
    departure = TextField('departure')
    arrival = TextField('arrival')
    dates = TextField('dates')

