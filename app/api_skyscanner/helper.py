import os
import sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../..')))
import requests
from app import logger
from skyscanner.skyscanner import FlightsCache, Transport

SKYSCANNER_API_KEY = os.environ.get("SKYSCANNER_API")


def cached_flights(departure=None, arrival=None, outbound_date='anytime', inbound_date='anytime'):
    try:
        origin_place = sky_airport_codes(location=departure)
        destination_place = sky_airport_codes(location=arrival)
        flights_cache_service = FlightsCache(SKYSCANNER_API_KEY)
        result = flights_cache_service.get_cheapest_price_by_date(
            market='US',
            currency='USD',
            locale='en-US',
            originplace=origin_place,
            destinationplace=destination_place,
            outbounddate=outbound_date,
            inbounddate=inbound_date
        ).parsed
        logger.info(u'cached flight search for departure {}; arrival {}'.format(departure, arrival))
        return result
    except requests.exceptions.HTTPError as e:
        logger.error(e)
        return None
    except IndexError as e:
        logger.error(u'lowest prices not found: {} for data departure {}; arrival {}'.format(e, departure, arrival))
        return None
    except ValueError as e:
        logger.error(u'lowest prices not found: {} for data departure {}; arrival {}'.format(e, departure, arrival))
        return None


def sky_airport_codes(location=None):
    result = autosuggest_service(query=location)
    return result["Places"][0]["PlaceId"]


def find_cheapest_fare(departure=None, arrival=None, outbound_date='anytime', inbound_date='anytime'):
    cached_data = cached_flights(
        departure=departure,
        arrival=arrival,
        outbound_date=outbound_date,
        inbound_date=inbound_date)
    if cached_data:
        try:
            _c = cheapest_flight_data_creator(cached_data)
            return _c

        except IndexError as e:
            logger.error(u'lowest prices not found: {} for data {}'.format(e, cached_data))
            return None

        except ValueError as e:
            logger.error(u'lowest prices not found: {} for data {}'.format(e, cached_data))
            return None


def cheapest_flight_data_creator(flight_data):
    _c = {}
    prices = [p["MinPrice"] for p in flight_data["Quotes"]]
    lowest_price_index = prices.index(min(prices))
    cheapest_flight_info = flight_data["Quotes"][int(lowest_price_index)]
    cheapest_flight_departure_carrier_id = cheapest_flight_info["OutboundLeg"]["CarrierIds"][0]
    cheapest_flight_arrival_carrier_id = cheapest_flight_info["InboundLeg"]["CarrierIds"][0]
    cheapest_flight_departure = None
    cheapest_flight_arrival = None

    # find flight name based on airport ID
    for k in flight_data["Carriers"]:
        if k["CarrierId"] == cheapest_flight_departure_carrier_id:
            cheapest_flight_departure = k["Name"]
        if k["CarrierId"] == cheapest_flight_arrival_carrier_id:
            cheapest_flight_arrival = k["Name"]

    # set up the data holder
    _c["price"] = min(prices)
    _c["is_direct"] = cheapest_flight_info["Direct"]
    _c["departure_flight"] = cheapest_flight_departure
    _c["arrival_flight"] = cheapest_flight_arrival
    _c["departure_date"] = cheapest_flight_info["OutboundLeg"]["DepartureDate"]
    _c["arrival_date"] = cheapest_flight_info["InboundLeg"]["DepartureDate"]
    return _c


def autosuggest_service(query):
    try:
        flights_sky_code_service = Transport(SKYSCANNER_API_KEY)
        result = flights_sky_code_service.location_autosuggest(
            market='US',
            currency='USD',
            locale='en-US',
            query=query
        ).parsed
        logger.info(u'airport codes based on query {}'.format(result))
        return result
    except requests.exceptions.HTTPError as e:
        logger.error(e)
        return None
    except IndexError as e:
        logger.error(u'autosuggest not found: {} for query {}'.format(e, query))
        return None
    except ValueError as e:
        logger.error(u'autosuggest not found: {} for query {}'.format(e, query))
        return None


if __name__ == "__main__":
    # for testing
    import pprint
    returned_data = find_cheapest_fare(
        departure="Prague, Czech Republic",
        arrival="New York",
        outbound_date='anytime',
        inbound_date='anytime')
    pprint.pprint(returned_data)

