import os
import sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../..')))
from app.mod_auth.models import User, FlightUserProfileDb


def get_all_users():
    _u = User.objects()
    return [x.email for x in _u]


def get_all_active_alerts():
    _f = FlightUserProfileDb.objects(status=True)
    holder = {}
    for _d in _f:
        if _d.user.email in holder:
            holder[str(_d.user.email)].append(
                {"user_id": str(_d.user.id),
                 "departure": str(_d.departure),
                 "arrival": str(_d.arrival),
                 "created": _d.created})
        else:
            holder[str(_d.user.email)] = [
                {"user_id": str(_d.user.id),
                 "departure": str(_d.departure),
                 "arrival": str(_d.arrival),
                 "created": _d.created}]

    return holder

if __name__ == "__main__":
    import pprint
    print(u'all active users')
    pprint.pprint(get_all_users())
    print(u'all active alerts')
    pprint.pprint(get_all_active_alerts())
